# save path on cd
function cd {
  builtin cd "$@"
  pwd > ~/.last_dir
}

# restore last saved path
if [ -f ~/.last_dir ]
  then cd "`cat ~/.last_dir`"
fi

export PATH="$PATH:$HOME/.local/bin"
export DOCKER_HOST=tcp://localhost:2375

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

alias pbcopy='xclip -selection clipboard' # this is mostly for wls
alias pbpaste='xclip -selection clipboard -o' #this is mostly for wls
export DISPLAY=:0
export PATH="$HOME/.nodenv/bin:$PATH"
eval "$(nodenv init -)"
export PROJECTS_DIR="/home/${whoami}/projects"

function goto {
  GOTO="$(echo -e "$1" | sed -e 's/^[[:space:]]*//')"
  GOTO_PATH="${PROJECTS_DIR}/${GOTO}"
  cd "$GOTO_PATH"
}