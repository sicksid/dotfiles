if &compatible
  set nocompatible
endif
" Add the dein installation directory into runtimepath
set runtimepath+=~/.cache/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.cache/dein')
  call dein#begin('~/.cache/dein')

  call dein#add('~/.cache/dein/repos/github.com/Shougo/dein.vim')
  call dein#add('mkitt/tabline.vim')
  call dein#add('Yggdroot/indentLine')
  call dein#add('djoshea/vim-autoread')
  call dein#add('lifepillar/vim-cheat40')
  call dein#add('tweekmonster/django-plus.vim')
  call dein#add('pallets/jinja')
  call dein#add('Shougo/deoplete.nvim')
  call dein#add('arcticicestudio/nord-vim')
  call dein#add('vim-python/python-syntax')
  call dein#add('vim-ruby/vim-ruby')
  call dein#add('pangloss/vim-javascript')
  call dein#add('Shougo/vimfiler.vim')
  call dein#add('Shougo/unite.vim')
  call dein#add('junegunn/fzf.vim', { 'depends': 'junegunn/fzf' })
  call dein#add('spolu/dwm.vim')
  call dein#add('vim-airline/vim-airline')
  call dein#add('vim-airline/vim-airline-themes')
  call dein#add('mhinz/vim-startify')
  call dein#add('mileszs/ack.vim')
  call dein#add('tpope/vim-fugitive')
  call dein#add('posva/vim-vue')
  call dein#add('ryanoasis/vim-devicons')
  if !has('nvim')
    call dein#add('roxma/nvim-yarp')
    call dein#add('roxma/vim-hug-neovim-rpc')
  endif

  call dein#end()
  call dein#save_state()
endif

let g:vimfiler_as_default_explorer = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:tablineclosebutton=1
let g:startify_relative_path=1
let g:startify_custom_header = ''
let g:ackprg = 'ag --nogroup --nocolor --column'
let g:deoplete#enable_at_startup = 1
let g:airline_theme='nord'
let g:cheat40_use_default = 0 "" remember to add cheat40.txt to ~/.vim
" let g:airline#extensions#tabline#buffer_nr_show = 1

let g:vue_disable_pre_processors = 1

au BufNewFile,BufRead *.html,*.htm,*.shtml,*.stm,*.jinja set ft=jinja
call vimfiler#custom#profile('default', 'context', {
    \'direction' : 'rightbelow',
    \'explorer': 1,
    \'split': 1,
    \'winwidth': 40,
    \'force_quit': 1,
    \'safe': 0,
    \})

filetype plugin indent on
syntax enable
set number
colorscheme nord
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set backspace=indent,eol,start
set rtp+=~/.fzf
set encoding=UTF-8
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*/node_modules/*
set lazyredraw
set cursorline!
set mouse=a
set clipboard=unnamed
set autoread
set swapfile
set dir=/tmp
set re=1
set pyx=0
set path+=**

nmap <Leader>f :VimFiler<CR>
vnoremap <leader>a "zy:execute "Ag ". @z<cr>
nnoremap <leader>a :Ag<Space>
nnoremap <leader>, :rightbelow terminal<CR>
nnoremap <leader><Left> :bp<CR>
nnoremap <leader><Right> :bn<CR>


function! GFilesFallback()
  let output = system('git rev-parse --show-toplevel') " Is there a faster way?
  let prefix = get(g:, 'fzf_command_prefix', '')
  if v:shell_error == 0
    exec "normal :" . prefix . "GFiles\<CR>"
  else
    exec "normal :" . prefix . "Files\<CR>"
  endif
  return 0
endfunction

nnoremap <c-p> :call GFilesFallback()<CR>